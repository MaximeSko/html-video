
function  ma_fonction(){
// Mon url

var url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png";
 
  // Appelez fetch(url) avec les option par défaut.
  // retourner la promesse dans une variable.
  var promesse = fetch(url);
 
  // Travailler avec la promesse:
  promesse
    .then(function(response) {
        console.log("OK! je reçois un objet qui est:");
        console.log(response);
 
        if(!response.ok) {
           throw new Error("HTTP error, status = " + response.status);
        }
        // Obtenir la promesse de blob:
        var myBlob_Leponge= response.blob();
        return myBlob_Leponge;
    })
    .then(function(myBlob) {
        console.log("OK! Blob:");
        console.log(myBlob);
 
        var objectURL = URL.createObjectURL(myBlob);
        var myImage = document.getElementById("myImg");
        myImage.src = objectURL;
    })
    .catch(function(error)  {
        console.log("Noooooon! Une Erreur c'est produite :");
        console.log(error);
    });
    
}

function player(){
    var objectURL = "https://www.youtube.com/embed/4qSHwpKMRBE";
    var Video = document.getElementById("myPlayer");
    Video.src = objectURL;
}
